---
title: OSI/ISO七层——TCP/IP四层
date: 2016-11-30 16:31:48
categories:
tags: TCP/IP
---

OSI：一个国际组织
ISO：OSI提出的网络的7层架构

TCP：传输控制协议<!-- more -->
IP：数据报协议
TCP/IP：TCP/IP为很大的一个协议族，里面包含了非常多的协议。如：TCP、IP、UDP、ARP、ICMP、RARP、IGMP......

他们的对应关系如下图所示：
![TCP/IP---OSI/ISO](http://oghakfaz6.bkt.clouddn.com/TCP%E5%92%8COSI.png)
LaoXianYu将会持续更新@Damon
